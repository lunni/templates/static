# `lunni/templates/static`

This template allows you to deploy a static site on your Lunni server, using
the [Caddy 2](https://caddyserver.com/) server.

---


## Quickstart

On your local machine, navigate to the directory in which you want to
create a project directory, and run the following command:

``` bash
lunni create static
```

Create a repository on your Git hosting, and then run the following commands:

``` bash
cd <project_name>
git init -b main
git add .
git commit -m "Init commit"
git remote add origin git@github.com:<username>/<project_name>.git
git push -u origin main
```

Finally, install the environment and the pre-commit hooks with:

```bash
make install
```

You are now ready to start development on your project! The CI/CD pipeline will
be triggered when you open a pull request, merge to main, or when you create a
new release.
