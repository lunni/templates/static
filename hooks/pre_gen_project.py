import re
import sys

PROJECT_NAME_REGEX = r"^[-a-zA-Z][-a-zA-Z0-9]+$"
project_name = "{{cookiecutter.project_name}}"
if not re.match(PROJECT_NAME_REGEX, project_name):
    print(
        "ERROR: The project name (%s) is not a valid repo name. Please do not use a _ and use - instead"
        % project_name
    )
    # Exit to cancel project
    sys.exit(1)
